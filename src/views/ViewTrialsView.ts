import {TrialReminderManager} from '../TrialReminderManager';

export class ViewTrialsView {
    private trialManager : TrialReminderManager = new TrialReminderManager();

    public async render() {
        let remindersContainer = document.getElementById("reminders_container");
        this.toggleSpinner(true);
        
        console.log("Rendering trials list markup...");
        const reminders = await this.trialManager.GetReminders();
        this.toggleSpinner(false);
        
        if (reminders.length == 0) {
            remindersContainer.innerText = "No reminders found.";
            return;
        }
        
        const markup = 
            `
            <ul class="mdl-list">
                ${reminders.map(((reminder, index) => `
                    <li class="mdl-list__item mdl-list__item--two-line" id=${reminder.Id}>
                        <span class="mdl-list__item-primary-content">
                            <span>${reminder.Name}</span>
                            <span class="mdl-list__item-sub-title">${reminder.ExpirationDate}</span>
                        </span>
                        <span class="mdl-list__item-secondary-content">
                            <a class="mdl-list__item-secondary-action" href="#">
                                <i class="material-icons" id='trial-${reminder.Id}'>cancel</i>
                            </a>
                        </span>
                    </li>
                `)).join('')}
            `;
        remindersContainer.innerHTML = markup;
    }
   
    public async removeTrial(id: string) {
        await this.trialManager.RemoveReminder(id);
        document.getElementById(id).remove();
    }

    private toggleSpinner(isVisible: boolean) {
        let spinner = document.getElementById("spinner");
        if (isVisible) {
            spinner.style.display = 'block';
        }
        else {
            spinner.style.display = 'none';
        }
    }
}
require('material-design-lite/dist/material.css');
require('material-design-lite/dist/material.js');
require('mdl-date-textfield/dist/mdl-date-textfield.min.css');
require('mdl-date-textfield/dist/mdl-date-textfield.min.js');
require('../scss/styles.scss');

import {ViewTrialsView} from './ViewTrialsView';
import {AddReminderView} from './AddReminderView';

document.addEventListener('DOMContentLoaded', () => {
    const addReminderView = new AddReminderView();
    const viewTrialsView = new ViewTrialsView();
    let reminderActionButtons: HTMLCollectionOf<Element>;

    document.getElementById('add_reminder_button').addEventListener("click", () => { 
        addReminderView.add();
    });
    
    document.getElementById('Add-Tab').addEventListener("click", () => { 
        document.getElementById("add-trial-panel").classList.remove("hide");
    });

    document.getElementById('View-Tab').addEventListener("click", () => { 
        viewTrialsView.render();
        document.getElementById("add-trial-panel").classList.add("hide");

        document.getElementById('reminders_container').addEventListener("click", (e) => 
        {
            let element = e.target as HTMLElement;
            if (element && element.id.startsWith("trial-")){
                const trialId = element.id.substring(6);
                viewTrialsView.removeTrial(trialId);    
            }
        });
    });
});
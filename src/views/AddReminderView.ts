import {TrialReminderManager} from '../TrialReminderManager';
import {Trial} from '../models/Trial';
import {Guid} from 'guid-typescript';

export class AddReminderView {
    public add() {
        let trialManager : TrialReminderManager = new TrialReminderManager();
    
        const trialName = (<HTMLInputElement>document.getElementById("trial_name")).value;
        const trialExpirationDate = (<HTMLInputElement>document.getElementById("trial_expiration_date")).value;
        
        const trial = new Trial(Guid.create().toString(), trialName, trialExpirationDate);
        trialManager.AddReminder(trial);
    }
}
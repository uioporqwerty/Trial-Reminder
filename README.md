# Trial Reminder
Trial reminder is a browser extension that notifies you when the trial period you signed up for is about to expire so that you can cancel it.

## Installation
1. git clone `git@gitlab.com:uioporqwerty/Trial-Reminder.git`
2. `npm install`
3. `npm run build`
4. Add packaged directory into chrome.